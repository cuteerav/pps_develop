<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}


// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class WebServices extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('login');
       $this->load->library("email");
        $this->load->library('form_validation');
       $this->load->helper('url');
        $this->perPageNum = 9;
    }

    /*
     * Login Request
     * Function -  Login
     */

    public function login_post() {
        $rest_json = file_get_contents("php://input");
        $postData = json_decode($rest_json, true);		
        if(NULL !=$postData['useremail'] && NULL !=$postData['password'] && NULL != $postData['deviceId'])
        {
          $username   =   $postData['useremail'];
          $password   =   $postData['password'];
          $deviceId   =   $postData['deviceId'];
          $registerId =   $postData['registerId'];
          $response = $this->login->clientApiLogin($username,$password);
			if ($response['success'] == true) 
			{	
                $userId = $response['data']->userId;
                if(!$this->_get_key_user($userId, $deviceId,$registerId))
                {
                    $apiKey = $this->createKey($response['data'], $password, $deviceId,$registerId);
                }
                // Else regenerate key for current user
                else
                {
                    $apiKey = $this->regenerateKey($userId, $deviceId,$registerId);                   
                }
                $response['data']->apiKey       =   $apiKey; 
                $output['status']         		= 	REST_Controller::HTTP_OK;
                $output['data']           		= 	$response['data'];
                $this->response($output, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response($response, REST_Controller::HTTP_CONFLICT);
            }
        }
        else
        {
          $this->response( ['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
        }
    }

	/* Add Store User Detail with Data */
	
	public function addStoreUser_post()
	{
		$rest_json 	= 	file_get_contents("php://input");
		$postData 	= 	json_decode($rest_json, true);
		if (!empty($postData))
		{
			$userData = array(
				'userName' => $postData['userName'],
				'userEmail' => $postData['userEmail'],
				'userPassword' => md5($postData['userPassword']),
				'userType' => $postData['userType'],
				'userStatus' => 1				
            );
			$storeTableData = array(
				'storeName' => $postData['storeName'],
				'storeEmail' => $postData['storeEmail'],
				'storeMobile' => $postData['storeMobile'],
				'storeAddress' => $postData['storeAddress'],
				'storeCity' => $postData['storeCity']							
            );
			$data = array('userData'=>$userData,'storeTableData'=>$storeTableData);
			$response = $this->login->insertData($data,$postData['usertableName'],$postData['storetableName']);
			
		}
	}
	
    /* Book Listing Based on Selected Genre Id Or Not */
    public function bookListing_post()
    {
      $rest_json = file_get_contents("php://input");
      $postData = json_decode($rest_json, true);
      $page = ($postData["page"]) ? $postData["page"] : 0;
      $page = $this->perPageNum * $page;

      if ($postData)
      {
        $genreId      = $postData['genreId'];
        $searchData   = $postData['searchData'];

        $response       = $this->login->getAllBooksData($this->perPageNum, $page,$genreId,$searchData);
        $totalCartItems = $this->login->getAddedCartItemsByUserId($postData['userId']);

        if ($response['success'] == true)
        {
          //  $output['status']         = REST_Controller::HTTP_OK;
            $output['totalRecords']   = $response['totalRecords'];
            $output['totalCartItems'] = $totalCartItems;
            $output['data']           = $response['data'];
            $this->response($output,REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response( ['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
        }
      }
      else
      {
          $this->response( ['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
      }
    }
    /****************Raman preet*******************/
    /* INGRAM Book Listing Based on Selected Genre Id Or Not */
    public function bookListingIngram_post()
    {
      $rest_json = file_get_contents("php://input");
      $postData = json_decode($rest_json, true);
      $page = ($postData["page"]) ? $postData["page"] : 0;
      $page = $this->perPageNum * $page;

      if ($postData)
      {
        $genreId      = $postData['genreId'];
        $searchData   = $postData['searchData'];

        $response       = $this->login->getAllBooksDataIngram($this->perPageNum, $page,$genreId,$searchData);
        $totalCartItems = $this->login->getAddedCartItemsByUserId($postData['userId']);

        if ($response['success'] == true)
        {
          //  $output['status']         = REST_Controller::HTTP_OK;
            $output['totalRecords']   = $response['totalRecords'];
            $output['totalCartItems'] = $totalCartItems;
            $output['data']           = $response['data'];
            $this->response($output,REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response( ['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
        }
      }
      else
      {
          $this->response( ['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
      }
    }

    /* Book Listing Based on Selected Genre Id Or Not */
    public function bookListing_get()
    {

      $page       = ($this->get('page')) ? $this->get('page') : 0;
      $page       = $this->perPageNum * $page;
      $genreId    = $this->get('genreId');
      $searchData = $this->get('searchData');
      // if ($postData)
      // {

        $response = $this->login->getAllBooksData($this->perPageNum, $page,$genreId,$searchData);

        if ($response['success'] == true)
        {
          //  $output['status']         = REST_Controller::HTTP_OK;
            $output['totalRecords']   = $response['totalRecords'];
            $output['data']           = $response['data'];
            $this->response($output,REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response( ['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
        }
      // }
      // else
      // {
      //     $this->response( ['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
      // }
    }

    /*  Get Book Store User Detail with BookStore User ID */
    public function bookStoreUserDetail_get()
    {
      $userId = $this->get('userId');
      if(isset($userId))
      {
          $response = $this->login->getBookStoreUserDetail($userId);
          //echo "<pre>"; print_r($response); echo "</pre>"; die;
          if ($response['success'] == true)
          {
            //  $output['status']         = REST_Controller::HTTP_OK;
              $output['data']           = $response['data'];
              $this->response($output,REST_Controller::HTTP_OK);
          }
          else
          {
              $this->response( ['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
          }
        }
        else
        {
            $this->response( ['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
        }
      }

      /*  Update Book Store User Detail with User ID */
      public function bookStoreUserDetail_put()
      {
        $userId = $this->put('userId');
        $deviceId = $this->put('deviceId');
        //if(NULL !=$this->put('userId') && NULL !=$this->put('FirstName') && NULL !=$this->put('LastName') && NULL !=$this->put('EmailAddress') && NULL !=$this->put('Address'))
        if(NULL !=$this->put('userId'))
        {
          $data = array(
                'bookStFName'      => $this->put('FirstName'),
                'bookStLName'      => $this->put('LastName'),
                'bookStAddress'    => $this->put('Address'),
                'bookStCity'       => $this->put('City'),
                'bookStCountry'    => $this->put('Country'),
                'bookStState'      => $this->put('State'),
                'bookStZip'        => $this->put('Zipcode'),
                'bookStEmail'      => $this->put('EmailAddress'),
                'bookStContactNo'  => $this->put('PhoneNumber'),
                'userEmail'        => $this->put('EmailAddress')
            );
            $response = $this->login->updateUserDetails($data,$userId,$deviceId);
            if ($response['success'] == true)
            {
              $this->response(['status'=>REST_Controller::HTTP_OK], REST_Controller::HTTP_OK);
            }
            else
            {
                //$this->response($output, REST_Controller::HTTP_CONFLICT);
                $this->response( ['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
            }
        }
        else
        {
            //$this->response($output, REST_Controller::HTTP_CONFLICT);
            $this->response( ['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
        }
      }

      /*  Get All Genre Listing and also with Genre Search */
      public function bookStoreGetAllGenres_get()
      {
            $searchData = $this->get('searchData');
            $response = $this->login->getAllGenres($searchData);
            if(!empty($response['data']))
            {
              foreach ( $response['data'] as $key => $value)
              {
                if($value->genreName == 'All Genres')
                {
                  $aa = $response['data'][$key];
                  unset($response['data'][$key]);
                }
              }
              array_unshift($response['data'],$aa);
            }
            if ($response['success'] == true)
            {
              //  $output['status']         = REST_Controller::HTTP_OK;
                $output['data']           = $response['data'];
                $this->response($output,REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response(['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
            }
        }

        /*  Get All Genre Listing and also with Genre Search */
        public function bookStoreGetBookDetail_get()
        {
              $bookid = $this->get('bookid');
              $response = $this->login->getBookDetailById($bookid);
              if ($response['success'] == true)
              {
                  //$output['status']         = REST_Controller::HTTP_OK;
                  $output           = $response['data'];
                  $this->response($output,REST_Controller::HTTP_OK);
              }
              else
              {
                  $this->response(['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
              }
        }

      /*  Update Book Store User Detail with User ID */
      public function forgotPassword_put()
      {
        $emailAddress = $this->put('emailAddress');
        $deviceId = $this->put('deviceId');
        if(NULL !=$this->put('emailAddress'))
        {
            $response = $this->login->forgotPassword($emailAddress,$deviceId);
          //  echo "<pre>"; print_r($response); echo "</pre>"; die;
            if ($response['success'] == true)
            {
              $this->response(['status'=>REST_Controller::HTTP_OK], REST_Controller::HTTP_OK);
            }
            else
            {
                //$this->response($output, REST_Controller::HTTP_CONFLICT);
                $this->response( ['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
            }
        }
      }

      /*  Get All Genre Listing and also with Genre Search */
      public function bookStoreGetCountriesData_get()
      {
            $countryid = $this->get('countryid');
            $response  = $this->login->getCountryStates($countryid);
            if ($response['success'] == true)
            {
                //$output['status']         = REST_Controller::HTTP_OK;
                $output['data']           = $response['data'];
                $this->response($output,REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response(['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
            }
    }

    /*  Get All Orders Listing of Particular User using user ID */
    public function bookStoreGetMyOrders_get()
    {
          $page = ($this->get('page')) ? $this->get('page') : 0;
          $page = $this->perPageNum * $page;
          $userId = $this->get('userId');
          if(NULL != $userId)
          {
            $response = $this->login->getallMyOrdersData($this->perPageNum, $page, $userId);
            if ($response['success'] == true)
            {
                //$output['status']         = REST_Controller::HTTP_OK;
              //  $output['totalOrders']   = $response['totalRecords'];
                $output['data']           = $response['data'];
                $this->response($output,REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response(['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
            }
          }
          else
          {
                $this->response(['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
          }
      }

      /*  Get All Orders Listing of Particular User using user ID */
      public function bookStoreGetMyOrderItemsByOrderId_get()
      {
            $page     = ($this->get('page')) ? $this->get('page') : 0;
            $page     = $this->perPageNum * $page;
            $orderId  = $this->get('orderId');
            if(NULL != $orderId)
            {
              $response = $this->login->getOrderItemsByOrderId($this->perPageNum,$page, $orderId);
              if ($response['success'] == true)
              {
                  $output['data']           = $response['data'];
                  $output['totalRecords']   = $response['totalRecords'];
                  $this->response($output,REST_Controller::HTTP_OK);
              }
              else
              {
                  $this->response(['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
              }
            }
            else
            {
                  $this->response(['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
            }
        }
    /*
     *  Logout
     */

     public function logout_get()
     {
          $Id       = $this->_get_key($this->get('ApiKey'))->id;
          $UserId   = $this->_get_key($this->get('ApiKey'))->UserId;
          $this->_delete_key($Id, $UserId);
          $this->response(['STATUS'=>REST_Controller::HTTP_OK, 'CONTENT'=>'Logout Successful'], REST_Controller::HTTP_OK);
     }

     public function bookStoreAddToCartProduct_post()
     {
       $rest_json = file_get_contents("php://input");
       $postData  = json_decode($rest_json, true);
       if ($postData)
       {
         $userId    = $postData['userId'];
         $bookid    = $postData['bookid'];
         $quantity  = $postData['quantity'];
         if(NULL != $userId && NULL != $bookid && NULL != $quantity )
         {
           $response = $this->login->saveAddTocartMyOrders($userId,$bookid,$quantity);
           if ($response['success'] == true)
           {
              $totalCartITems  = $this->login->getAddedCartItemsByUserId($userId);
            //   $output['status']         = REST_Controller::HTTP_OK;
               $output['totalCartItems']  = $totalCartITems;
               $output['data']            = $response['data'];
               $this->response($output,REST_Controller::HTTP_OK);
           }
           else
           {
               $this->response(['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
           }
         }
         else
         {
            $this->response(['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
         }
       }
       else
       {
             $this->response(['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
       }
     }

     public function bookStoreUpdateAddToCartProductQty_post()
     {
       $rest_json = file_get_contents("php://input");
       $postData  = json_decode($rest_json, true);
       if ($postData)
       {
         $userId    = $postData['userId'];
         $bookid    = $postData['bookid'];
         $quantity  = $postData['quantity'];
         $bkId      = $postData['bkId'];
         if(NULL != $userId && NULL != $bookid && NULL != $quantity && NULL != $bkId )
         {
           $response = $this->login->updateAddTocartProductQty($userId,$bookid,$quantity,$bkId);
           if ($response['success'] == true)
           {
              $totalCartITems  = $this->login->getAddedCartItemsByUserId($userId);
            //   $output['status']         = REST_Controller::HTTP_OK;
               $output['totalCartItems']  = $totalCartITems;
               $output['data']            = $response['data'];
               $this->response($output,REST_Controller::HTTP_OK);
           }
           else
           {
               $this->response(['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
           }
         }
         else
         {
            $this->response(['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
         }
       }
       else
       {
             $this->response(['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
       }
     }

     public function bookStoreGetAllProductFromAddToCart_get()
     {
       $userId = $this->get('userId');
       if(NULL != $userId)
       {
         $response        = $this->login->getAllAddedProductsFromAddtoCartByUserId($userId);

         if ($response['success'] == true)
         {
            // $output['status']         = REST_Controller::HTTP_OK;
             $output['totalItems']    = $response['num_results'];
             $output['totalItemPrice']= $response['totalItemPrice'];
             $output['totalNewItemPrice']= $response['totalNewItemPrice'];
             $output['data']          = $response['data'];
             $this->response($output,REST_Controller::HTTP_OK);
         }
         else
         {
             $this->response(['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
         }
       }
       else
       {
         $this->response(['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
       }
     }

     public function bookStoreDeleteProductFromAddToCart_get()
     {
       $bkId = $this->get('bkId');
       if(NULL != $bkId)
       {
         $response = $this->login->deleteBookfromAddTocart($bkId);
         if ($response['success'] == true)
         {
            // $output['status']          = REST_Controller::HTTP_OK;
             $output['data']            = 'removed';
             $this->response($output,REST_Controller::HTTP_OK);
         }
         else
         {
             $this->response(['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
         }
       }
       else
       {
         $this->response(['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
       }
     }

     /*  Get All Genre Listing and also with Genre Search */
     public function bookStoreSaveMyOrders_post()
     {
           $userId      = $this->post('userId');
           $bookStoreId = $this->post('bookStoreId');
           if(NULL != $userId)
           {
             $response = $this->login->saveMyOrders($userId,$bookStoreId);
             if ($response['success'] == true)
             {
                // $output['status']         = REST_Controller::HTTP_OK;
                 $output['data']           = $response['data'];
                 $this->response($output,REST_Controller::HTTP_OK);
             }
             else
             {
                 $this->response(['status'=>REST_Controller::HTTP_CONFLICT], REST_Controller::HTTP_CONFLICT);
             }
           }
           else
           {
                 $this->response(['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
           }
       }

       public function updatePassword_post() {
           $rest_json = file_get_contents("php://input");
           $postData = json_decode($rest_json, true);
           if ($postData) {
               $passwordExist = $this->login->checkPassword($postData['oldPassword'], $postData['userId']);
               if ($passwordExist['success'] == false) {
                   $output['success'] = 'false';
                   $output['error_message'] = $passwordExist['error_message'];
                   $this->response($output, REST_Controller::HTTP_CONFLICT);
               }
               $response = $this->login->updatePassword($postData);
               if ($response['success'] == true) {
                   $output['success'] = 'true';
                   $output['success_message'] = $response['success_message'];
                   $this->response($output);
               } else {
                   $output['success'] = 'false';
                   $output['error_message'] = $response['error_message'];
                   $this->response($output, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
               }
           } else {
               $output['success'] = 'false';
               $output['error_message'] = 'Check your parameter.';
               $this->response($output, REST_Controller::HTTP_BAD_REQUEST);
           }
       }


       public function checkOutNewAddedBooks_post() {
           $rest_json = file_get_contents("php://input");
           $postData = json_decode($rest_json, true);
           if ($postData) {
               $response = $this->login->checkOutNewAddedBooks($postData);
               if ($response['success'] == true) {
                   $output['success'] = ' true';
                   $output['success_message'] = $response['success_message'];
                   $this->response($output);
               } else {
                   $output['success'] = 'false';
                   $output['error_message'] = $response['error_message'];
                   $this->response($output, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
               }
           } else {
               $output[' success'] = 'false';
               $output['error_message'] = 'Check your parameter. ';
               $this->response($output, REST_Controller::HTTP_BAD_REQUEST);
           }
       }
       // get latest notification

    public function bookNotificationHistory_get()
    {
        $response = $this->login->getLatestBooksData();
        //print_r($response); die;
        if ($response['success'] == true)
        {
          //  $output['status']         = REST_Controller::HTTP_OK;
            $output['totalRecords']   = $response['totalRecords'];
            $output['data']           = $response['data'];
            $this->response($output,REST_Controller::HTTP_OK);
        }
        else
        {

        $this->response( ['status'=>REST_Controller::HTTP_BAD_REQUEST], REST_Controller::HTTP_CONFLICT);
        }
    }

}

//Book my saloon RESTful API using PHP & CodeIgniter
